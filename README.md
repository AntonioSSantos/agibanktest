# Automacao AGIBANK

Automacao teste API AgiBank

## Getting Started

Importar projeto Gradle

### Prerequisites
Java 11

Gradle 6
## Running the tests

IDE: execute o arquivo RunnerTest.java como JUnit test

Linha de comando: gradlew.bat clean test

Gerar relatorios allure: gradlew.bat allureReport 

- _Relatorio deve ser gerado apenas apos a execucao dos testes_

Visualizar relatorio: gradlew.bat allureServe 

- _O relatorio sera aberto no navegador padrao do sistema_

Executar direto: gradlew.bat clean test allureReport allureServe

## Authors

* **Antonio de Sousa Santos** - (https://gitlab.com/antonio86)
